class galaxy::deploy {

  package { 'unzip':
    name => 'unzip',
    ensure => 'present',
  }

  file { "/etc/sudoers.d/galaxy":
    owner   => root,
    group   => root,
    mode    => '0440',
    source => 'puppet:///modules/galaxy/galaxy.sudoers',
  }

  apt::source { 'nectar':
    location          => 'http://115.146.94.144/apt-repo',
    release           => 'nectar',
    repos             => 'main',
    include_src       => true,
  }


  #This is a hack but somehow xMilvView breaks this package
  #So just install it here

  package { 'dictionaries-common':
    name => 'dictionaries-common',
    ensure => 'present',
  }
}


