class galaxy {
   
  $username = 'galaxy'
  
  group { $username:
    ensure => 'present',
  }
  
  user  { $username:
    ensure => 'present',
    comment => 'Galaxy User',
    gid => $username,
    home => "/home/$username",
    shell => '/bin/bash',
    managehome => true, 
  }	

  file { "/home/$username/":
    ensure  => directory,
    owner   => $username,
    group   => $username,
    mode    => 755,
    require => [ User[$username], Group[$username] ]
  }

#TODO: Move to deploy
  file { "/home/$username/.ssh":
    ensure  => directory,
    owner   => $username,
    group   => $username,
    mode    => 700,
    require => [ User[$username], Group[$username] ]
  }
 
  file { "/home/$username/.ssh/authorized_keys":
    ensure  => present,
    owner   => $username,
    group   => $username,
    mode    => 600,
    source => 'puppet:///modules/galaxy/authorized_keys',
    require => [ User[$username], Group[$username] ]
  }


  file { "/mnt/var/":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => 755,
  }

  file { "/mnt/var/$username/":
    ensure  => directory,
    owner   => $username,
    group   => $username,
    mode    => 755,
    require => [ User[$username], Group[$username] ]
  }

}

