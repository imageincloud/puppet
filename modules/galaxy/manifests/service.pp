class galaxy::service {

  $galaxy_user = 'galaxy'

  file { "/etc/init.d/galaxy":
    owner   => root,
    group   => root,
    mode    => '755',
    source => 'puppet:///modules/galaxy/galaxy.rc',
  }

  service { 'galaxy':
     path => '/etc/init.d/',
     enable => true,
     ensure => 'stopped',
     require =>  File["/etc/init.d/galaxy"],
  }

  file { "/mnt/var/$galaxy_user/logs":
    ensure  => directory,
    owner   => $galaxy_user,
    group   => $galaxy_user,
    mode    => 755,
  }

}

