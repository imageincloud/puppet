class galaxy::webserver {
  
  package { 'apache2':
    name => 'apache2',
    ensure => 'present',
  }

  # Enable modules

  file { "/etc/apache2/mods-enabled/proxy.load":
    ensure => link,
    target => "/etc/apache2/mods-available/proxy.load",
    require => Package['apache2'],
  }

  file { "/etc/apache2/mods-enabled/proxy.conf":
    ensure => link,
    target => "/etc/apache2/mods-available/proxy.conf",
    require => Package['apache2'],
  }

  file { "/etc/apache2/mods-enabled/proxy_http.load":
    ensure => link,
    target => "/etc/apache2/mods-available/proxy_http.load",
    require => Package['apache2'],
  }

  file { "/etc/apache2/mods-enabled/rewrite.load":
    ensure => link,
    target => "/etc/apache2/mods-available/rewrite.load",
    require => Package['apache2'],
  }

  # Galaxy configuration site
  file { "/etc/apache2/sites-available/default":
    owner   => root,
    group   => root,
    mode    => '0644',
    source => 'puppet:///modules/galaxy/galaxy.site',
    require => Package['apache2'],
  }

}
