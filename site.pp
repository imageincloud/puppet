
node default {
     include galaxy
     include galaxy::webserver
     include galaxy::service
     include galaxy::db
     include galaxy::deploy
}
